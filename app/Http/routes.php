<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return Redirect::to('/post');
});

Route::get('/add-post',['middleware' => 'auth', function () {

    $cat = DB::table('category')
        ->select( 'id' , 'name' )->get();
    $tag = DB::table('tag')
        ->select( 'id' , 'name' )->get();
    return View::make('addPost', array('tags' => $tag , 'cats' => $cat ));
}]);



Route::post('/add-comment',['middleware' => 'auth', function () {

    $data = Input::all();
    $author_id = Auth::user();

    DB::table('comments')->insert(
        ['comment' => $data['comment'] , 'author_id' => $author_id->id , 'posts_id' =>  $data['posts_id'] , 'created_at' => date("Y-m-d H:i:s")]
    );
    return Redirect::to('/post/'.$data['posts_id']);

}]);

Route::get('/post/cat/{cat}', function ($cat) {


    $is_login = Auth::check();
    $post = DB::table('posts')
        ->leftJoin('users', 'users.id', '=', 'posts.author_id')
        ->select('posts.id', 'posts.contents', 'posts.title' , 'posts.created_at' , 'users.name')
        ->orderBy('posts.created_at', 'desc')
        ->where('posts.cat_id' , $cat)
        ->get();
    $cat = DB::table('category')
        ->select( 'id' , 'name' )->get();
    $tag = DB::table('tag')
        ->select( 'id' , 'name' )->get();
    return View::make('posts', array('posts' => $post , 'is_login' => $is_login , 'cats' => $cat ,'tags' => $tag ));
});

Route::get('/post/tag/{tag}', function ($tag) {


    $is_login = Auth::check();


    $post = DB::table('posts')
        ->join('posts_tag' , 'posts_tag.posts_id' , '=' , 'posts.id')
        ->leftJoin('users', 'users.id', '=', 'posts.author_id')
        ->select('posts.id', 'posts.contents', 'posts.title' , 'posts.created_at' , 'users.name')
        ->orderBy('posts.created_at', 'desc')
        ->where('posts_tag.tag_id' , $tag)
        ->get();

    $cat = DB::table('category')
        ->select( 'id' , 'name' )->get();
    $tag = DB::table('tag')
        ->select( 'id' , 'name' )->get();
    return View::make('posts', array('posts' => $post , 'is_login' => $is_login , 'cats' => $cat ,'tags' => $tag ));
});

Route::get('/edit-post/{post}',['middleware' => 'auth', function ($post) {

    $cat = DB::table('category')
        ->select( 'id' , 'name' )
        ->get();
    $tag = DB::table('tag')
        ->select( 'id' , 'name' )->get();
    $postTags = DB::table('posts_tag')
        ->select(  'tag_id' )
        ->where('posts_id' , $post->id )
        ->get();
    $post_tag=array();
    foreach($postTags as $postTag)
    {
        $post_tag[]=$postTag->tag_id;
    }

    return View::make('editPost', array('tags' => $tag , 'cats' => $cat , 'postTag' => $post_tag , 'post' => $post ));
}]);

/*
 *Route to Controller for Post
 */

Route::get('/post', 'Post\PostController@listOfAllModels');
Route::get('/post/search', 'Post\PostController@listOfAllModels_search');
Route::get('/post/{post}', 'Post\PostController@jsonOfModel');
Route::get('/post/{post}/delete', ['middleware' => 'auth','uses' => 'Post\PostController@deletePost']);
Route::post('/post',['middleware' => 'auth','uses' =>  'Post\PostController@insertPost']);
Route::post('/post/{post}/update', ['middleware' => 'auth','uses' => 'Post\PostController@updatePost']);
Route::get('/user/{user}/post', 'Post\PostController@listPostOfUser');

/*
 *Route to Controller for Login
 */

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

