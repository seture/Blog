<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use App\Models\Login;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


class LoginController extends Controller
{
    public function loginForm(){
        return view('login_form');
    }

    function RandomString($length,$characters) {

        $randomString = '';
        for ($i=0; $i<$length; $i++) {
            $randomString .= $characters[rand(0,strlen($characters)-1)];
        }
        return $randomString;
    }

    public function login(Request $request){

        $ltl  = Config::get('constants.LOGIN_TOKEN_LENGTH');
        $ltch = Config::get('constants.LOGIN_TOKEN_CHARACTER');

        $username = $request->username;
        $password = $request->password;
        $user = User::where('username', '=', $username)->first();
        $token = array();
        if (!empty($user) && md5($password) == $user->password){

            //-------------------------------------------------------
            $token['token'] = $this->RandomString($ltl,$ltch);

            $token_json_encode = json_encode($token);
			
            //-------------------------------------------------------

            $login = new Login();
            $login->user_id     = $user->id;
            $login->token       = $token['token'];
           
            $login->save();
            //-------------------------------------------------------
            return response()->json(array('status' => 'OK' ,'data' => $token_json_encode));
            //-------------------------------------------------------
        } else {
            abort(403);
        }
    }
}