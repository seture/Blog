<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use  App\Exceptions\CustomControllerException;

class UserController extends Controller
{
    const ERR_DATA_INVALID = 1;

    public function getUserFiles(User $user)
    {
        return $user->fileuser_usermodel;
    }

    public function getUser(User $user)
    {
        return $user;
    }

    public function getUserRoles(User $user)
    {
        return $user->userrole_usermodel;
    }

    public function deleteUser(User $user)
    {
        if($user->delete())
            return response()->json(array('status' => 'OK'));
        else
            return response()->json(array('status' => 'error'));
    }

    public function insertUserForm()
    {
        return view('user.insert_u_c');
    }
    public function insertUser(Request $request)
    {
        try
        {
            $this->validate($request,[
                'name'     => 'required',
                'password' => 'required'
            ]);

            $name     = $request->input('name');
            $password = $request->input('password');

            $user = new User();
            $user->name     = $name;
            $user->password = $password;
            $user->save();
            return response()->json(array('status' => 'OK' ,'data' => $user->id));

        }
        catch(\Exception $e)
        {
            $c = new CustomControllerException('data insertion is failed' , self::ERR_DATA_INVALID );
            throw $c;
        }

    }

    public function updaterUserForm(User $user)
    {
        return view('user.update_u_c',['user'=>$user]);
    }

    public function updateUser(Request $request, User $user)
    {
        try
        {
            $this->validate($request,[
                'name'     => '',
                'password' => ''
            ]);

            $username = $request->name;
            $password = $request->password;

            if (!empty($username)){
                $user->name = $username;
            }
            if (!empty($password)){
                $user->password = $password;
            }
            $user->save();
            return response()->json(array('status' => 'OK'));
        }
        catch(\Exception $e)
        {
            $c = new CustomControllerException('data update is failed' , self::ERR_DATA_INVALID );
            throw $c;
        }
    }
    public function listOfAllModels()
    {
        $file = User::all();
        return response()->json( array('data' => $file ));
    }
    public function jsonOfModel(User $user)
    {
        return response()->json(array('data' => $user));
    }
    public function listRolesOfUser(User $user)
    {
        $roles = $user->roles;
        return response()->json(array('data' => $roles));
    }
    public function insertUserRole(User $user ,$role)
    {
        try
        {
            $user->roles()->attach($role);
            return response()->json(array('status' => 'OK'));
        }
        catch(\Exception $e)
        {
            $c = new CustomControllerException('appending role to user is failed. it must be already exist.' , self::ERR_DATA_INVALID );
            throw $c;
        }
    }

}