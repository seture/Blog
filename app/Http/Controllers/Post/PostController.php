<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use  App\Exceptions\CustomControllerException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\Models\User;


class PostController extends Controller
{
    const ERR_DATA_INVALID = 1;
    protected $layout = 'layouts.master';
    public function listOfAllModels()
    {
        $is_login = Auth::check();
        $post = DB::table('posts')
            ->leftJoin('users', 'users.id', '=', 'posts.author_id')
            ->select('posts.id', 'posts.contents', 'posts.title' , 'posts.created_at' , 'users.name')
            ->orderBy('posts.created_at', 'desc')
            ->get();
        $cat = DB::table('category')
            ->select( 'id' , 'name' )->get();
        $tag = DB::table('tag')
            ->select( 'id' , 'name' )->get();
        return View::make('posts', array('posts' => $post , 'is_login' => $is_login , 'cats' => $cat ,'tags' => $tag ));
    }

    public function listOfAllModels_search(Request $request)
    {
        $is_login = Auth::check();
        $post = DB::table('posts')
            ->leftJoin('users', 'users.id', '=', 'posts.author_id')
            ->select('posts.id', 'posts.contents', 'posts.title' , 'posts.created_at' , 'users.name')
            ->where('posts.contents' , 'like' , "%$request->search%")
            ->orderBy('posts.created_at', 'desc')
            ->get();
        $cat = DB::table('category')
            ->select( 'id' , 'name' )->get();
        $tag = DB::table('tag')
            ->select( 'id' , 'name' )->get();
        return View::make('posts', array('posts' => $post , 'is_login' => $is_login , 'cats' => $cat ,'tags' => $tag ));
    }

	public function updatePost(Request $request , Post $post)
	{
        try
        {
            $this->validate($request,[
                'title'     => '',
                'contents' => '',
                'cat' => ''
            ]);
            $title = $request->title;
            $contents = $request->contents;
            $cat = $request->cat;
            $tags = $request->tags;

            if (!empty($title)){
                $post->title = $title;
            }
            if (!empty($contents)){
                $post->contents = $contents;
            }
            if (!empty($cat)){
                $post->cat_id = $cat;
            }
            $post->save();

            DB::table('posts_tag')->where('posts_id', $post->id )->delete();
            if($tags)
                foreach($tags as $tag)
                {
                    DB::table('posts_tag')->insert([
                        'posts_id' => $post->id ,
                        'tag_id' => $tag
                    ]);
                }
            return Redirect::to('/post');
        }
        catch(\Exception $e)
        {
            $c = new CustomControllerException('data update is failed' , self::ERR_DATA_INVALID );
            throw $c;
        }	
	}

    public function jsonOfModel(Post $post)
    {
        $is_login = Auth::check();
        $user = User::find($post->author_id);
        $post->author_name = $user->name;
        $cat = DB::table('category')
            ->select( 'name' )->where('id' , $post->cat_id)
            ->get();
        $postTags = DB::table('posts_tag')->leftJoin('tag', 'tag.id', '=', 'posts_tag.tag_id')
            ->select(  'tag.name' )
            ->where('posts_id' , $post->id )
            ->get();
        $comments = DB::table('comments')->Join('posts', 'posts.id', '=', 'comments.posts_id')
            ->leftJoin('users', 'users.id', '=', 'comments.author_id')
            ->select(  'comments.comment' , 'users.name' , 'comments.created_at' )
            ->where('posts_id' , $post->id )
            ->get();
        $postTags1 = DB::table('posts_tag')
            ->select(  'tag_id' )
            ->where('posts_id' , $post->id )->get();
        $arr= array();
        foreach($postTags1 as $postTags2)
        {
            $arr[]=$postTags2->tag_id;
        }

        $relatedPosts = DB::table('posts_tag')
            ->select(  'posts_tag.posts_id' , 'posts.title'  )
            ->leftJoin('posts', 'posts.id', '=', 'posts_tag.posts_id')
            ->whereIn('posts_tag.tag_id'  , $arr )
            ->where('posts_tag.posts_id' , '<>'  , $post->id  )
            ->groupBy('posts_tag.posts_id')
            ->having(DB::raw('count(posts_tag.posts_id)'), '>',2)
            ->get();

        return View::make('post', array('posts' => $post , 'tags' => $postTags , 'cats' => $cat[0] , 'is_login' => $is_login ,'comments' => $comments , 'relatedPosts' => $relatedPosts ));
    }

	public function insertPost(Request $request)
    {
        try
        {
            $author_id = Auth::user();
            $this->validate($request,[
                'title'     => 'required',
                'contents' => 'required',
                'cat' => 'required',
                'tags' => ''
            ]);
            $title     = $request->input('title');
            $contents = $request->input('contents');
            $tags=array();
            if($request->exists('tags'))
                $tags = $request->input('tags');
            $cat = $request->input('cat');
            $post = new Post();
            $post->title     = $title;
            $post->contents = $contents;
			$post->author_id = $author_id->id;
            $post->cat_id = $cat;
            $post->save();
            foreach($tags as $tag)
            {
                DB::table('posts_tag')->insert([
                    'posts_id' => $post->id ,
                    'tag_id' => $tag
                ]);
            }
            return Redirect::to('/post');
        }
        catch(\Exception $e)
        {
            $c = new CustomControllerException('data insertion is failed' , self::ERR_DATA_INVALID );
            throw $c;
        }
    }
	
	public function deletePost(Post $post)
    {
        if($post->delete())
        {
            DB::table('posts_tag')->where('posts_id', $post->id )->delete();
            return Redirect::to('/post');
        }
        else
            return response()->json(array('status' => 'error'));
    }
}