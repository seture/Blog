<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//cdn.ckeditor.com/4.5.3/basic/ckeditor.js"></script>

</head>
<body>

<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Add Post
                    </div>
                </div>
                <div class="panel-body">
        <!-- Blog Post Content Column -->

                    <div class="col-lg-8 col-lg-offset-2 col-md-12">
            <form class="form-horizontal" method="POST" action="/post">
                <div class="form-group">

                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" class="form-control"  name="title" id="title" >
                        </div>

                        <div class="form-group">
                            <label for="contents">Content:</label>
                            <textarea id="contents" class="form-control" placeholder="Enter text ..." name="contents" id="contents"></textarea>
                            <script>
                                CKEDITOR.replace( 'contents' );
                            </script>
                        </div>

                        <div class="form-group">
                            <label for="sel1">Category:</label>
                            <select class="form-control" id="sel1" name="cat">
                                @foreach ($cats as $cat)
                                    <option value="{{ $cat->id }}" >{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="sel1">Tags:</label>
                                @foreach ($tags as $tag)
                            <br><input type="checkbox"  value="{{ $tag->id }}" name="tags[]">{{ $tag->name }}
                                @endforeach
                        </div>

                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
                        </div>


    </div>
    <!-- /.row -->
                </div></div></div>
    <hr>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>