
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>

<body>


<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">


                @if ($is_login == 1)
                <li>
                    <a href="/auth/logout">Logout</a>
                </li>
                @else
                <li>
                    <a href="/auth/login">Login</a>
                </li>
                <li>
                    <a href="/auth/register">Register</a>
                </li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<br>
<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
                Blog Test
                <small>It's just a test project</small>
            </h1>

            @if ($is_login == 1)
            <a href="/add-post" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Post</a>
            @endif
            <hr>
            @foreach ($posts as $post)
                <!-- Blog Post -->
                <h2>
                    <a href="/post/{{ $post->id }}">{{ $post->title }}</a>
                </h2>
                <p class="lead">
                    by {{ $post->name }}
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on {{ $post->created_at }}</p>
                <hr>
                <p>{{ $post->contents }}</p>
                <a class="btn btn-primary" href="/post/{{ $post->id }}">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                @if ($is_login == 1)
                <a href="/edit-post/{{ $post->id }}" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span> Edit Post</a>
                <a href="/post/{{ $post->id }}/delete" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete Post</a>
                @endif

                <hr>
            @endforeach
        </div>

        <!-- Blog Sidebar Widgets Column -->
        <br><br>
        <div class="col-md-4">

            <!-- Blog Search Well -->
            <div class="well">
                <h4>Blog Search</h4>
                <div class="input-group">
                    <form method="get" action="/post/search/" >
                    <input type="text" class="form-control" name="search">
                        <span class="input-group-btn">
                            <input type="submit" class="btn btn-default" value="search">


                        </span>
                    </form>
                </div>
                <!-- /.input-group -->
            </div>

            <!-- Blog Categories Well -->
            <div class="well">
                <h4>Blog Categories</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            @foreach ($cats as $cat)
                            <li><a href="/post/cat/{{ $cat->id }}">{{ $cat->name }}</a>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
                <!-- /.row -->
            </div>

            <div class="well">
                <h4>Blog Tags</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            @foreach ($tags as $tag)
                            <li><a href="/post/tag/{{ $tag->id }}">{{ $tag->name }}</a>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
                <!-- /.row -->
            </div>


</div>
    </div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>
