<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">


                @if ($is_login == 1)
                <li>
                    <a href="/auth/logout">Logout</a>
                </li>
                @else
                <li>
                    <a href="/auth/login">Login</a>
                </li>
                <li>
                    <a href="/auth/register">Register</a>
                </li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<br>

    <!-- Page Content -->
    <div class="container">


        <div class="row">
            <!-- Blog Post Content Column -->
            <div class="col-lg-8">
                <br>
                <!-- Blog Post -->
                <!-- Title -->
                <h1>{{ $posts->title }}</h1>
                <!-- Author -->
                <p class="lead">
                    by {{ $posts->author_name }}
                </p>
                <hr>
                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span>{{ $posts->created_at }}</p>
                <hr>
                <hr>
                <!-- Post Content -->
                {{ $posts->contents }}
                <hr>
                <hr>
                <!-- Blog Category -->

                Category : <span class="label label-primary">{{ $cats->name }}</span>
                <hr>

                <hr>
                <!-- Blog Tags -->

                Tags :
                @foreach ($tags as $tag)

                 <span class="label label-info">{{ $tag->name }}</span>

                @endforeach

                <hr>
                @if ($is_login == 1)
                <!-- Blog Comments -->
                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="post" action="/add-comment">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="comment"></textarea>
                        </div>
                        <input type="hidden" name="posts_id" value="{{ $posts->id }}" >
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <hr>
                @endif
                <!-- Posted Comments -->
                <!-- Comment -->
                <h4>Comments:</h4>
                @foreach ($comments as $comment)
                <div class="media">

                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{ $comment->name }}
                        <small>{{ $comment->created_at }}</small>
                        </h4>
                        {{ $comment->comment }}
                    </div>

                </div>
                @endforeach
            </div>




            <div class="col-md-4">
<br><br>
                <div class="well">
                    <h4>Related Posts</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                @foreach ($relatedPosts as $relatedPost)
                                <li><a href="/post/{{ $relatedPost->posts_id }}">{{ $relatedPost->title }}</a>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>

                </div>




        </div>
        <hr>
    </div>
    <!-- /.container -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>